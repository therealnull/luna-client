package com.jagex.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequencer;

public class MusicController {

	public static int currentSong = 0;

	public static Sequencer sequencer;

	public static void playSong(int id) {
		try {
			if (sequencer == null)
				sequencer = MidiSystem.getSequencer();

			if (id == 65535) {
				if (sequencer.isOpen()) {
					sequencer.stop();
					sequencer.close();
				}
				currentSong = id;
				return;
			}

			else if (currentSong != id) {
				if (sequencer.isOpen())
					sequencer.close();

				InputStream is = new BufferedInputStream(new FileInputStream(new File("./data/Music/" + id + ".mid")));
				if (sequencer.isOpen())
					sequencer.close();
				sequencer.setSequence(MidiSystem.getSequence(is));
				sequencer.open();
				sequencer.start();

				currentSong = id;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
